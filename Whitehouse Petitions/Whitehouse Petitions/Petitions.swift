//
//  Petitions.swift
//  Whitehouse Petitions
//
//  Created by macbook on 14.10.21.
//

import Foundation

struct Petitions: Codable {
    var results: [Petition]
}
