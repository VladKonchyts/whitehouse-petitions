//
//  Petition.swift
//  Whitehouse Petitions
//
//  Created by macbook on 14.10.21.
//

import Foundation

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}
